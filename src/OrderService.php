<?php

namespace Drupal\commerce_cart_advanced;

use Drupal\commerce\ConfigurableFieldManagerInterface;
use Drupal\commerce_order\Entity\OrderTypeInterface;
use Drupal\entity\BundleFieldDefinition;

use Drupal\Core\Entity\EntityFieldManagerInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;

/**
 * Provides functionality related to orders.
 */
class OrderService {

  use StringTranslationTrait;

  /**
   * The commerce configurable field manager.
   *
   * @var \Drupal\commerce\ConfigurableFieldManagerInterface
   */
  protected $configurableFieldManager;

  /**
   * The entity field manager.
   *
   * @var \Drupal\Core\Entity\EntityFieldManagerInterface
   */
  protected $entityFieldManager;

  /**
   * Constructs a new OrderService object.
   *
   * @param \Drupal\commerce\ConfigurableFieldManagerInterface $configurable_field_manager
   *   The commerce configurable field manager.
   * @param \Drupal\Core\Entity\EntityFieldManagerInterface $entity_field_manager
   *   The entity field manager.
   */
  public function __construct(
    ConfigurableFieldManagerInterface $configurable_field_manager,
    EntityFieldManagerInterface $entity_field_manager
  ) {
    $this->configurableFieldManager = $configurable_field_manager;
    $this->entityFieldManager = $entity_field_manager;
  }

  /**
   * Returns whether the non current cart field is installed on the order type.
   *
   * @param \Drupal\commerce_order\Entity\OrderTypeInterface $order_type
   *   The order type to check.
   *
   * @return bool
   *   TRUE if the field is installed, FALSE otherwise.
   */
  public function isNonCurrentFieldInstalled(OrderTypeInterface $order_type) {
    $definitions = $this->entityFieldManager->getFieldDefinitions(
      'commerce_order',
      $order_type->id()
    );

    foreach ($definitions as $definition) {
      if ($definition->getType() !== 'boolean') {
        continue;
      }
      if ($definition->getName() !== COMMERCE_CART_ADVANCED_NON_CURRENT_FIELD_NAME) {
        continue;
      }

      return TRUE;
    }

    return FALSE;
  }

  /**
   * Installs the field for marking non-current carts to the given order type.
   *
   * @param \Drupal\commerce_order\Entity\OrderTypeInterface $order_type
   *   The order type to which to install the field.
   */
  public function installNonCurrentField(OrderTypeInterface $order_type) {
    $field_definition = BundleFieldDefinition::create('boolean')
      ->setTargetEntityTypeId('commerce_order')
      ->setTargetBundle($order_type->id())
      ->setName(COMMERCE_CART_ADVANCED_NON_CURRENT_FIELD_NAME)
      ->setLabel($this->t('Non-current cart'))
      ->setDescription($this->t('Indicates whether an order is a non-current cart.'));

    $this->configurableFieldManager->createField($field_definition);
  }

}

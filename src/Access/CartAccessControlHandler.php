<?php

namespace Drupal\commerce_cart_advanced\Access;

use Drupal\commerce_cart\CartSessionInterface;
use Drupal\commerce_order\Entity\OrderInterface;
use Drupal\entity\EntityAccessControlHandler;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Session\AccountInterface;

/**
 * Access control handler for cart orders.
 *
 * The Commerce Cart module checks for order permissions when managing order
 * using the admin UI. Permissions to create, view, update or delete carts are
 * granted without checks because it is the most common use case. Since the
 * Commerce Cart Advanced module introduces permissions for cart order
 * operations, this access control handler can be used for checking access in
 * such cases.
 *
 * This is not a standard access control handler that will be automatically
 * called as the default handler of the order entity; it therefore is defined as
 * a service and needs to be called as such wherever required.
 *
 * @I Provide and implement create cart permission
 *    type     : feature
 *    priority : normal
 *    labels   : access
 *    notes    : Create cart permission will be checked when adding purchasable
 *               entities to the cart and in the `Add to cart` form.
 *
 * @I Provide and implement delete cart permission
 *    type     : feature
 *    priority : normal
 *    labels   : access
 *    notes    : When a cart is emptied of all its items, it is not deleted by
 *               Commerce Cart. A `delete` permission could be used in a
 *               `Remove cart` button feature.
 */
class CartAccessControlHandler extends EntityAccessControlHandler {

  /**
   * The cart session.
   *
   * @var \Drupal\commerce_cart\CartSessionInterface
   */
  protected $cartSession;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Constructs a new CartAccessControlHandler object.
   *
   * @param \Drupal\commerce_cart\CartSessionInterface $cart_session
   *   The cart session.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   */
  public function __construct(
    CartSessionInterface $cart_session,
    EntityTypeManagerInterface $entity_type_manager
  ) {
    parent::__construct($entity_type_manager->getDefinition('commerce_order'));

    $this->cartSession = $cart_session;
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * {@inheritdoc}
   *
   * This method is exactly the same as the one provided by the core handler;
   * the only difference is that it calls implementations of
   * `hook_commerce_cart_access` instead of
   * `hook_commerce_order_access`. Otherwise we would inadvertedly call the
   * wrong hook with results that would be meant for different use cases - see
   * class comments.
   */
  public function access(
    EntityInterface $entity,
    $operation,
    AccountInterface $account = NULL,
    $return_as_object = FALSE
  ) {
    $account = $this->prepareUser($account);
    $langcode = $entity->language()->getId();

    if ($operation === 'view label' && $this->viewLabelOperation == FALSE) {
      $operation = 'view';
    }

    // If an entity does not have a UUID, either from not being set or from not
    // having them, use the 'entity type:ID' pattern as the cache $cid.
    $cid = $entity->uuid() ?: $entity->getEntityTypeId() . ':' . $entity->id();

    // If the entity is revisionable, then append the revision ID to allow
    // individual revisions to have specific access control and be cached
    // separately.
    if ($entity instanceof RevisionableInterface) {
      /** @var $entity \Drupal\Core\Entity\RevisionableInterface */
      $cid .= ':' . $entity->getRevisionId();
    }

    $return = $this->getCache($cid, $operation, $langcode, $account);
    if ($return !== NULL) {
      // Cache hit, no work necessary.
      return $return_as_object ? $return : $return->isAllowed();
    }

    // Invoke hook_commerce_cart_access(). Hook results take precedence over
    // overridden implementations of EntityAccessControlHandler::checkAccess().
    // Entities that have checks that need to be done before the hook is invoked
    // should do so by overriding this method.
    //
    // We grant access to the entity if both of these conditions are met:
    // - No modules say to deny access.
    // - At least one module says to grant access.
    $access = $this->moduleHandler()
      ->invokeAll(
        'commerce_cart_access',
        [$entity, $operation, $account]
      );
    $return = $this->processAccessHookResults($access);

    // Also execute the default access check except when the access result is
    // already forbidden, as in that case, it can not be anything else.
    if (!$return->isForbidden()) {
      $return = $return->orIf(
        $this->checkAccess($entity, $operation, $account)
      );
    }

    $result = $this->setCache($return, $cid, $operation, $langcode, $account);
    return $return_as_object ? $result : $result->isAllowed();
  }

  /**
   * {@inheritdoc}
   */
  protected function checkAccess(
    EntityInterface $entity,
    $operation,
    AccountInterface $account
  ) {
    /** @var \Drupal\commerce_order\Entity\OrderInterface $entity */

    $account = $this->prepareUser($account);

    if (!$this->orderIsUnlockedCart($entity)) {
      return AccessResult::neutral()->addCacheableDependency($entity);
    }

    return $this->checkEntityOwnerPermissions($entity, $operation, $account)
      ->addCacheableDependency($entity);
  }

  /**
   * {@inheritdoc}
   */
  protected function checkEntityOwnerPermissions(
    EntityInterface $entity,
    $operation,
    AccountInterface $account
  ) {
    /** @var \Drupal\commerce_order\Entity\OrderInterface $entity */

    // The "any" permission grants access regardless of the entity owner.
    $any_result = AccessResult::allowedIfHasPermissions(
      $account,
      [
        "$operation any commerce_order cart",
        "$operation any {$entity->bundle()} commerce_order cart",
      ],
      'OR'
    );

    if ($any_result->isAllowed()) {
      return $any_result;
    }

    // Otherwise, we check if the user is the owner.
    if ($this->isOwner($entity, $account)) {
      $own_result = AccessResult::allowedIfHasPermissions(
        $account,
        [
          "$operation own commerce_order cart",
          "$operation own {$entity->bundle()} commerce_order cart",
        ],
        'OR'
      );
    }
    else {
      $own_result = AccessResult::neutral()->cachePerPermissions();
    }

    // The "own" permission is based on the current user's ID, so the result
    // must be cached per user.
    return $own_result->cachePerUser();
  }

  /**
   * Returns whether the given user is the owner of the given order.
   *
   * @param \Drupal\commerce_order\Entity\OrderInterface $order
   *   The order.
   * @param \Drupal\Core\Session\AccountInterface $account
   *   The user account.
   */
  protected function isOwner(OrderInterface $order, AccountInterface $account) {
    // Determine whether the user is the owner of the cart.
    $is_owner = $account->id() == $order->getCustomerId();

    // Additionally, if the user is anonymous the IDs should still be matching
    // but they would always be 0 making it possible to view other anonymous
    // user's carts. Check that the cart is available in the current user's
    // session to verify ownership.
    if ($account->isAnonymous()) {
      $is_owner = $is_owner && $this->cartSession->hasCartId($order->id());
    }

    return $is_owner;
  }

  /**
   * Returns whether the order is an unlocked cart.
   *
   * Cart permissions are not applicable if the order is not a cart or if it is
   * locked.
   */
  protected function orderIsUnlockedCart(OrderInterface $order) {
    $is_cart = $order->getState()->value === 'draft' && $order->cart;
    if (!$is_cart) {
      return FALSE;
    }

    return !$order->isLocked();
  }

}

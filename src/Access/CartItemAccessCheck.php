<?php

namespace Drupal\commerce_cart_advanced\Access;

use Drupal\commerce_order\Entity\OrderItemInterface;
use Drupal\Core\Entity\EntityAccessControlHandlerInterface;
use Drupal\Core\Routing\Access\AccessInterface;
use Drupal\Core\Session\AccountInterface;

/**
 * Checks access for routes updating a cart item.
 *
 * @I Provide a cart item access control handler
 *    type     : bug
 *    priority : normal
 *    labels   : access-control, cart-item
 *    notes    : A cart item access control handler will allow other modules to
 *               implement their custom logic, similar to the cart access
 *               control handler.
 */
class CartItemAccessCheck implements AccessInterface {

  /**
   * The cart access control handler.
   *
   * @var \Drupal\Core\Entity\EntityAccessControlHandlerInterface
   */
  protected $cartAccessHandler;

  /**
   * Constructs a new CartItemAccessCheck object.
   *
   * @param \Drupal\Core\Entity\EntityAccessControlHandlerInterface $cart_access_handler
   *   The cart access control handler.
   */
  public function __construct(
    EntityAccessControlHandlerInterface $cart_access_handler
  ) {
    $this->cartAccessHandler = $cart_access_handler;
  }

  /**
   * Checks if the user can update a cart item.
   *
   * @param \Drupal\Core\Session\AccountInterface $account
   *   The account for which to check access.
   * @param Drupal\commerce_order\Entity\OrderItemInterface $commerce_order_item
   *   The order item.
   *
   * @return \Drupal\Core\Access\AccessResultInterface
   *   The access result.
   */
  public function access(
    AccountInterface $account,
    OrderItemInterface $commerce_order_item
  ) {
    // We grant access if the user has `update` access to the cart.
    return $this->cartAccessHandler->access(
      $commerce_order_item->getOrder(),
      'update',
      $account,
      TRUE
    );
  }

}

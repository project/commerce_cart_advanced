<?php

namespace Drupal\commerce_cart_advanced;

use Drupal\commerce_cart\CartProviderInterface;
use Drupal\commerce_store\Entity\StoreInterface;
use Drupal\Core\Session\AccountInterface;

/**
 * Creates and loads carts for anonymous and authenticated users.
 *
 * On top of the default cart provider interface, it defines functions for
 * getting current carts only.
 *
 * Note the following important difference in the public API methods.
 *
 * - When requesting multiple carts (`getCarts`, `getCartIds`,
 *   `getCurrentCarts`, `getCurrentCartIds`), carts from all stores will be
 *   selected if a store is not provided.
 * - When requesting an individual cart (`getCart`, `getCartId`,
 *   `getCurrentCart`, `getCurrentCartId`), the cart for the current store will
 *   be selected if not is provided.
 *
 * This is because when requesting multiple carts we don't know the intention;
 * we may be in a marketplace where we do want to display all carts from
 * different stores.
 *
 * @see \Drupal\commerce_cart\CartProviderInterface
 */
interface AdvancedCartProviderInterface extends CartProviderInterface {

  /**
   * Gets current cart orders for the given user.
   *
   * @param \Drupal\Core\Session\AccountInterface $account|null
   *   The user account. If NULL, the current user is assumed.
   * @param \Drupal\commerce_store\Entity\StoreInterface $store|null
   *   The store. If NULL, current carts for all stores will be returned.
   *
   * @return \Drupal\commerce_order\Entity\OrderInterface[]
   *   A list of current cart orders.
   */
  public function getCurrentCarts(
    AccountInterface $account = NULL,
    StoreInterface $store = NULL
  );

  /**
   * Gets current cart order IDs for the given user.
   *
   * @param \Drupal\Core\Session\AccountInterface $account|null
   *   The user. If NULL, the current user is assumed.
   * @param \Drupal\commerce_store\Entity\StoreInterface $store|null
   *   The store. If NULL, current carts for all stores will be returned.
   *
   * @return int[]
   *   A list of current cart order IDs.
   */
  public function getCurrentCartIds(
    AccountInterface $account = NULL,
    StoreInterface $store = NULL
  );

  /**
   * Gets the current cart order for the given order type, store and user.
   *
   * @param string $order_type
   *   The order type ID i.e. bundle.
   * @param \Drupal\commerce_store\Entity\StoreInterface $store|null
   *   The store. If NULL, the current store is assumed.
   * @param \Drupal\Core\Session\AccountInterface $account|null
   *   The user. If NULL, the current user is assumed.
   *
   * @return \Drupal\commerce_order\Entity\OrderInterface|null
   *   The cart order, or NULL if none found.
   */
  public function getCurrentCart(
    $order_type,
    StoreInterface $store = NULL,
    AccountInterface $account = NULL
  );

  /**
   * Gets the current cart order ID for the given order type, store and user.
   *
   * @param string $order_type
   *   The order type ID i.e. bundle.
   * @param \Drupal\commerce_store\Entity\StoreInterface $store|null
   *   The store. If NULL, the current store is assumed.
   * @param \Drupal\Core\Session\AccountInterface $account|null
   *   The user. If NULL, the current user is assumed.
   *
   * @return int|null
   *   The cart order ID, or NULL if none found.
   */
  public function getCurrentCartId(
    $order_type,
    StoreInterface $store = NULL,
    AccountInterface $account = NULL
  );

}

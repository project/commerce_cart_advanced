<?php

namespace Drupal\commerce_cart_advanced\Controller;

use Drupal\commerce_order\Entity\OrderItemInterface;

use Drupal\Core\Entity\EntityAccessControlHandlerInterface;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\Session\AccountInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Controller for the cart item pages.
 *
 * Provides a page for managing an individual cart item.
 *
 * @I Display order item adjustments on the cart item page
 *    type     : improvement
 *    priority : high
 *    labels   : cart-item, form
 */
class CartItemController extends ControllerBase {

  /**
   * The cart access control handler.
   *
   * @var \Drupal\Core\Entity\EntityAccessControlHandlerInterface
   */
  protected $cartAccessHandler;

  /**
   * Constructs a new CartItemController object.
   *
   * @param \Drupal\Core\Entity\EntityAccessControlHandlerInterface $cart_access_handler
   *   The cart access control handler.
   */
  public function __construct(
    EntityAccessControlHandlerInterface $cart_access_handler
  ) {
    $this->cartAccessHandler = $cart_access_handler;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('commerce_cart_advanced.cart_access_control_handler')
    );
  }

  /**
   * Checks access.
   *
   * Confirms that the user has access to the cart item.
   *
   * @param \Drupal\Core\Routing\RouteMatchInterface $route_match
   *   The route match.
   * @param \Drupal\Core\Session\AccountInterface $account
   *   The current user account.
   *
   * @return \Drupal\Core\Access\AccessResult
   *   The access result.
   */
  public function checkAccess(
    RouteMatchInterface $route_match,
    AccountInterface $account
  ) {
    /** @var \Drupal\commerce_order\Entity\OrderInterface $cart */
    $cart_item = $route_match->getParameter('cart_item');

    // We grant access if the user has `update` access to the cart.
    return $this->cartAccessHandler->access(
      $cart_item->getOrder(),
      'update',
      $account,
      TRUE
    );
  }

  /**
   * Renders the form for managing an individual cart item.
   *
   * @param \Drupal\commerce_order\Entity\OrderItemInterface $cart_item
   *   The cart item.
   *
   * @return array
   *   A render array.
   */
  public function cartItemPage(OrderItemInterface $cart_item) {
    return $this->entityFormBuilder()->getForm(
      $cart_item,
      'commerce_cart_advanced_cart_item'
    );
  }

}

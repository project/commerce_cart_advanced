<?php

namespace Drupal\commerce_cart_advanced\Plugin\views\field;

use Drupal\views\Plugin\views\field\FieldPluginBase;
use Drupal\views\Plugin\views\field\UncacheableFieldHandlerTrait;
use Drupal\views\ResultRow;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;

/**
 * Defines a form element for linking to the cart item page.
 *
 * @ViewsField("commerce_cart_advanced_cart_item_link")
 */
class CartItemLink extends FieldPluginBase {

  use UncacheableFieldHandlerTrait;

  /**
   * {@inheritdoc}
   */
  public function clickSortable() {
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function getValue(ResultRow $row, $field = NULL) {
    return '<!--form-item-' . $this->options['id'] . '--' . $row->index . '-->';
  }

  /**
   * Form constructor for the views form.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   */
  public function viewsForm(array &$form, FormStateInterface $form_state) {
    // Make sure we do not accidentally cache this form.
    $form['#cache']['max-age'] = 0;

    // The view is empty, abort.
    if (empty($this->view->result)) {
      unset($form['actions']);
      return;
    }

    $form[$this->options['id']]['#tree'] = TRUE;

    // Create a link for each order item.
    foreach ($this->view->result as $row_index => $row) {
      $order_item = $this->getEntity($row);
      $form[$this->options['id']][$row_index] = [
        '#type' => 'link',
        '#title' => t('Edit'),
        '#attributes' => ['class' => ['edit-order-item']],
        '#url' => Url::fromRoute(
          'commerce_cart_advanced.cart_item_page',
          ['commerce_order_item' => $order_item->id()]
        ),
      ];
    }
  }

  /**
   * {@inheritdoc}
   */
  public function query() {
    // Do nothing.
  }

}

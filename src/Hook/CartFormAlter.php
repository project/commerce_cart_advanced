<?php

namespace Drupal\commerce_cart_advanced\Hook;

use Drupal\views\Form\ViewsForm;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\StringTranslation\TranslationInterface;
use Drupal\Core\Url;

/**
 * Alters cart forms.
 */
class CartFormAlter {

  use StringTranslationTrait;

  /**
   * The configuration factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The messenger.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;

  /**
   * The current route match service.
   *
   * @var \Drupal\Core\Routing\ResettableStackedRouteMatchInterface
   */
  protected $routeMatch;

  /**
   * Constructs a new CartItemFormAlter object.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The configuration factory.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\Core\Messenger\MessengerInterface $messenger
   *   The messenger.
   * @param \Drupal\Core\Routing\RouteMatchInterface $current_route_match
   *   The current route match service.
   * @param \Drupal\Core\StringTranslation\TranslationInterface $string_translation
   *   The string translation service.
   */
  public function __construct(
    ConfigFactoryInterface $config_factory,
    EntityTypeManagerInterface $entity_type_manager,
    MessengerInterface $messenger,
    RouteMatchInterface $current_route_match,
    TranslationInterface $string_translation
  ) {
    $this->configFactory = $config_factory;
    $this->entityTypeManager = $entity_type_manager;
    $this->messenger = $messenger;
    $this->routeMatch = $current_route_match;

    // Injections required by the `StringTranslationTrait` trait.
    $this->stringTranslation = $string_translation;
  }

  /**
   * Cart form alterations.
   *
   * Currently, we do the following alterations.
   * - Add a "View all carts" link if we are on the single cart page.
   * - Add a "View cart" link if we are not on the single cart page.
   * - Add a "Save for later" button if the cart is current.
   * - Add a "Restore" button if the cart is non-current.
   *
   * @param array &$form
   *   Nested array of form elements that comprise the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   * @param string $form_id
   *   String representing the name of the form itself.
   */
  public function alterForm(
    array &$form,
    FormStateInterface $form_state,
    $form_id
  ) {
    $cart = $this->getCart($form, $form_state, $form_id);
    if (!$cart) {
      return;
    }

    $this->buildCartPageLink($form);
    $this->buildSingleCartPageLink($form, $cart->id());

    // Only add the "Save for later" and "Restore" if the module is configured
    // to display non-current carts.
    $config = $this->configFactory->get('commerce_cart_advanced.settings');
    if (!$config->get('display_non_current_carts')) {
      return;
    }

    $field = $cart->get(COMMERCE_CART_ADVANCED_NON_CURRENT_FIELD_NAME);
    if ($field->isEmpty() || !$field->value) {
      $this->buildCurrentCart($form, $cart->id());
      return;
    }

    $this->buildNonCurrentCart($form, $cart->id());
  }

  /**
   * Adds a link to view all carts, if we are on a single cart page.
   *
   * @param array &$form
   *   Nested array of form elements that comprise the form.
   */
  protected function buildCartPageLink(array &$form) {
    // We only add the link to the default cart page that lists all carts if we
    // are on an individual cart page.
    $route_name = $this->routeMatch->getRouteName();
    if ($route_name !== 'commerce_cart_advanced.single_cart_page') {
      return;
    }

    $form['actions']['view_all_link'] = [
      '#type' => 'link',
      '#title' => $this->t('View all carts'),
      '#weight' => 7,
      '#url' => Url::fromRoute('commerce_cart.page'),
    ];
  }

  /**
   * Adds a link to the single cart page, if we are not on it already.
   *
   * @param array &$form
   *   Nested array of form elements that comprise the form.
   * @param string|int $order_id
   *   The ID of the cart order.
   */
  protected function buildSingleCartPageLink(array &$form, $order_id) {
    // We only add the link to the individual cart page if we are not on it.
    $route_name = $this->routeMatch->getRouteName();
    if ($route_name === 'commerce_cart_advanced.single_cart_page') {
      return;
    }

    $form['actions']['view_link'] = [
      '#type' => 'link',
      '#title' => $this->t('View cart'),
      '#weight' => 7,
      '#url' => Url::fromRoute(
        'commerce_cart_advanced.single_cart_page',
        ['cart' => $order_id]
      ),
    ];
  }

  /**
   * Adds a "Save for later" button.
   *
   * @param array &$form
   *   Nested array of form elements that comprise the form.
   * @param string|int $order_id
   *   The ID of the cart order.
   */
  protected function buildCurrentCart(array &$form, $order_id) {
    $form['actions']['save_for_later'] = [
      '#type' => 'submit',
      '#value' => $this->t('Save for later'),
      '#weight' => 6,
      '#submit' => array_merge(
        $form['#submit'],
        ['_commerce_cart_advanced_cart_form_submit']
      ),
      '#order_id' => $order_id,
    ];
  }

  /**
   * Adds a "Restore" button.
   *
   * @param array &$form
   *   Nested array of form elements that comprise the form.
   * @param string|int $order_id
   *   The ID of the cart order.
   */
  protected function buildNonCurrentCart(array &$form, $order_id) {
    $form['actions']['restore'] = [
      '#type' => 'submit',
      '#value' => $this->t('Restore'),
      '#weight' => 6,
      '#submit' => array_merge(
        $form['#submit'],
        ['_commerce_cart_advanced_cart_form_submit__restore']
      ),
      '#order_id' => $order_id,
    ];
  }

  /**
   * Submit callback for marking a cart as non-current.
   *
   * @param array &$form
   *   Nested array of form elements that comprise the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   */
  public function submitSave(
    array &$form,
    FormStateInterface $form_state
  ) {
    $order_storage = $this->entityTypeManager->getStorage('commerce_order');
    $cart = $order_storage->load(
      $form_state->getTriggeringElement()['#order_id']
    );

    $cart->set(COMMERCE_CART_ADVANCED_NON_CURRENT_FIELD_NAME, TRUE);
    $order_storage->save($cart);

    $this->messenger->addMessage($this->t('Cart has been saved for later'));
    $form_state->setRedirect('commerce_cart.page');
  }

  /**
   * Submit callback for restoring a cart as current.
   *
   * @param array &$form
   *   Nested array of form elements that comprise the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   */
  public function submitRestore(
    array &$form,
    FormStateInterface $form_state
  ) {
    $order_storage = $this->entityTypeManager->getStorage('commerce_order');
    $cart = $order_storage->load(
      $form_state->getTriggeringElement()['#order_id']
    );

    $cart->set(COMMERCE_CART_ADVANCED_NON_CURRENT_FIELD_NAME, FALSE);
    $order_storage->save($cart);

    $this->messenger->addMessage($this->t('Cart has been restored'));
    $form_state->setRedirect('commerce_cart.page');
  }

  /**
   * Detects whether the form is a cart form and returns the cart.
   *
   * These form alterations are only meant to be used in cart forms powered by
   * Views - the default in Commerce Cart. These may be cart forms in the
   * global cart page provided by Commerce Cart or in the single cart page
   * provided by Commerce Cart Advanced.
   *
   * @param array &$form
   *   Nested array of form elements that comprise the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   * @param string $form_id
   *   String representing the name of the form itself.
   *
   * @return \Drupal\commerce_order\Entity\OrderInterface|null
   *   The cart entity, or NULL if the form is not a Views cart form.
   */
  protected function getCart(
    array &$form,
    FormStateInterface $form_state,
    $form_id
  ) {
    if (!($form_state->getFormObject() instanceof ViewsForm)) {
      return;
    }

    $view = reset($form_state->getBuildInfo()['args']);
    if (empty($view->result)) {
      return;
    }
    $tags = explode(',', $view->storage->get('tag'));
    // `explode()` will return FALSE if there is no tag.
    if ($tags === FALSE) {
      return;
    }
    $tags = array_map('trim', $tags);
    if (!in_array('commerce_cart_form', $tags)) {
      return;
    }

    // We know that view forms are built on the base ID plus arguments.
    return $this->entityTypeManager
      ->getStorage('commerce_order')
      ->load($view->argument['order_id']->value[0]);
  }

}

<?php

namespace Drupal\commerce_cart_advanced\Hook;

use Drupal\commerce_order\Entity\OrderInterface;
use Drupal\commerce_order\Entity\OrderItemInterface;
use Drupal\commerce_price\NumberFormatterFactoryInterface;
use Drupal\commerce_price\Price;

use Drupal\Core\Entity\ContentEntityFormInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\StringTranslation\TranslationInterface;
use Drupal\Core\Url;

/**
 * Alters the form for updating or removing a cart item.
 *
 * We would normally create a custom form mode and form class for building the
 * form; however, there seems to be an issue with the `inline_entity_form`
 * module and managing entity reference fields does not work with a custom form
 * mode. That is required, for example, by the Group Commerce Split module to
 * manage a cart item's split items.
 *
 * It does work with the `default` form mode, however, and we are using that
 * form mode with the default form class until the issue gets resolved; we
 * therefore we do form alterations instead of providing a form class.
 *
 * The current approach has the limitation that we don't take full advantage of
 * a custom form mode that would allow the fields that are displayed on the cart
 * item page to be managed via the UI.
 *
 * @I Resolve issue with cart item form and inline entity form widget
 *    type     : improvement
 *    priority : normal
 *    labels   : cart-item, form
 */
class CartItemFormAlter {

  use StringTranslationTrait;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The number formatter.
   *
   * @var \CommerceGuys\Intl\Formatter\NumberFormatterInterface
   */
  protected $numberFormatter;

  /**
   * The current route match service.
   *
   * @var \Drupal\Core\Routing\ResettableStackedRouteMatchInterface
   */
  protected $routeMatch;

  /**
   * Constructs a new CartItemFormAlter object.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\commerce_price\NumberFormatterFactoryInterface $number_formatter_factory
   *   The number formatter factory.
   * @param \Drupal\Core\Routing\RouteMatchInterface $current_route_match
   *   The current route match service.
   * @param \Drupal\Core\StringTranslation\TranslationInterface $string_translation
   *   The string translation service.
   */
  public function __construct(
    EntityTypeManagerInterface $entity_type_manager,
    NumberFormatterFactoryInterface $number_formatter_factory,
    RouteMatchInterface $current_route_match,
    TranslationInterface $string_translation
  ) {
    $this->entityTypeManager = $entity_type_manager;
    $this->routeMatch = $current_route_match;
    $this->stringTranslation = $string_translation;

    // @I Make currency display format configurable
    //    type     : improvement
    //    priority : low
    //    notes    : Could be configurable order item type. Should include
    //               current display and minimum fraction digits.
    $this->numberFormatter = $number_formatter_factory->createInstance();
    $this->numberFormatter->setMaximumFractionDigits(2);
  }

  /**
   * Implements hook_form_alter().
   *
   * @param array &$form
   *   Nested array of form elements that comprise the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   * @param string $form_id
   *   String representing the name of the form itself.
   */
  public function alterForm(
    array &$form,
    FormStateInterface $form_state,
    $form_id
  ) {
    if (!$this->isCartItemForm($form, $form_state, $form_id)) {
      return;
    }

    $order_item = $this->getOrderItem($form_state);

    $this->buildTable($order_item, $form, $form_state);
    $this->buildFields($form);
    $this->buildActionButtons($form, $order_item);
  }

  /**
   * Submit callback for removing the order item.
   *
   * @param array &$form
   *   Nested array of form elements that comprise the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   */
  public static function submitRemove(
    array &$form,
    FormStateInterface $form_state
  ) {
    $values = $form_state->getValues();

    $order_item = $form_state->getFormObject()->getEntity();
    $order = $order_item->getOrder();

    \Drupal::service('commerce_cart.cart_manager')->removeOrderItem(
      $order,
      $order_item
    );

    \Drupal::service('messenger')->addMessage(
      t('Your shopping cart item has been removed.')
    );
    self::submitRedirect($order, $form_state);
  }

  /**
   * Submit callback for updating the order item.
   *
   * @param array &$form
   *   Nested array of form elements that comprise the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   */
  public static function submitUpdate(
    array &$form,
    FormStateInterface $form_state
  ) {
    $order_item = $form_state->getFormObject()->getEntity();
    $order = $order_item->getOrder();

    $quantity = $form_state->getValues()['order_item'][0]['quantity']['data'];
    if ($quantity <= 0) {
      \Drupal::service('commerce_cart.cart_manager')->removeOrderItem(
        $order,
        $order_item
      );
    }
    elseif ($order_item->getQuantity() != $quantity) {
      $order_item->setQuantity($quantity);
      \Drupal::service('commerce_cart.cart_manager')->updateOrderItem(
        $order,
        $order_item
      );
    }

    \Drupal::service('messenger')->addMessage(
      t('Your shopping cart item has been updated.')
    );
    self::submitRedirect($order, $form_state);
  }

  /**
   * Set the redirect to the order item's cart page.
   *
   * @param \Drupal\commerce_order\Entity\OrderInterface $order
   *   The order item's cart order.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   */
  public static function submitRedirect(
    OrderInterface $order,
    FormStateInterface $form_state
  ) {
    $form_state->setRedirect(
      'commerce_cart_advanced.single_cart_page',
      ['cart' => $order->id()]
    );
  }

  /**
   * Builds the order item table.
   *
   * We make it similar to the default table that lists all order items on the
   * cart page.
   *
   * @param \Drupal\commerce_order\Entity\OrderItemInterface $order_item
   *   The order item for which to build the table.
   * @param array &$form
   *   Nested array of form elements that comprise the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   *
   * @I Allow setting a View to provide the cart item table
   *    type     : improvement
   *    priority : normal
   *    labels   : cart-item
   */
  protected function buildTable(
    OrderItemInterface $order_item,
    array &$form,
    FormStateInterface $form_state
  ) {
    $form['order_item'] = [
      '#type' => 'table',
      '#header' => [
        ['data' => $this->t('Item'), 'class' => 'cart-table-item'],
        ['data' => $this->t('Price'), 'class' => 'cart-table-price'],
        ['data' => $this->t('Quantity'), 'class' => 'cart-table-quantity'],
        ['data' => $this->t('Total'), 'class' => 'cart-table-price'],
      ],
      '#weight' => -100,
    ];

    // Product variation.
    // @I Review and test behavior for other purchasable entities
    //    type     : bug
    //    priority : normal
    //    labels   : cart-item, form
    $variation = $order_item->getPurchasedEntity();
    $variation_build = $this->entityTypeManager
      ->getViewBuilder($variation->getEntityTypeId())
      ->view($variation, 'cart');

    // Unit price.
    // @I Review and test whether we need to get the calculated price
    //    type     : bug
    //    priority : normal
    //    labels   : cart-item, form
    $unit_price_build = $this->buildPrice($order_item->getUnitPrice());

    // Total price.
    $total_price_build = $this->buildPrice($order_item->getTotalPrice());

    $form['order_item'][] = [
      'item' => [
        'data' => $variation_build,
        '#wrapper_attributes' => ['class' => 'cart-table-item'],
      ],
      'unit_price' => [
        'data' => $unit_price_build,
        '#wrapper_attributes' => ['class' => 'cart-table-price'],
      ],
      'quantity' => [
        'data' => $this->buildEditQuantity($order_item),
        '#wrapper_attributes' => ['class' => 'cart-table-quantity'],
      ],
      'total_price' => [
        'data' => $total_price_build,
        '#wrapper_attributes' => ['class' => 'cart-table-price'],
      ],
    ];
  }

  /**
   * Manipulates form elements for entity fields.
   *
   * We are not using a custom form mode (see class comments); we therefore
   * remove the standard fields that are displayed/edited in the order item
   * table; that should cover the most common requirements. Further
   * customizations can be done in a custom module.
   *
   * @param array &$form
   *   Nested array of form elements that comprise the form.
   */
  protected function buildFields(array &$form) {
    unset($form['purchased_entity']);
    unset($form['quantity']);
    unset($form['unit_price']);
  }

  /**
   * Manipulates form elements for action buttons.
   *
   * @param array &$form
   *   Nested array of form elements that comprise the form.
   * @param \Drupal\commerce_order\Entity\OrderItemInterface $order_item
   *   The form's order item.
   */
  protected function buildActionButtons(
    array &$form,
    OrderItemInterface $order_item
  ) {
    // Let's name the `Save` button to `Update` to match the title used in the
    // Cart page.
    $form['actions']['submit']['#value'] = $this->t('Update');

    // Add a submit callback for updating the order item.
    $form['actions']['submit']['#submit'][] = 'Drupal\commerce_cart_advanced\Hook\CartItemFormAlter::submitUpdate';

    // Add a button to remove the order item.
    $form['actions']['remove'] = [
      '#type' => 'submit',
      '#name' => 'remove',
      '#value' => $this->t('Remove'),
      '#weight' => 10,
      '#submit' => ['Drupal\commerce_cart_advanced\Hook\CartItemFormAlter::submitRemove'],
    ];

    // Add a link to go back to the cart.
    $form['actions']['back'] = [
      '#type' => 'link',
      '#title' => $this->t('Cancel'),
      '#url' => Url::fromRoute(
        'commerce_cart_advanced.single_cart_page',
        ['cart' => $order_item->getOrderId()]
      ),
      '#weight' => 15,
    ];
  }

  /**
   * Renders the given price object.
   *
   * @param \Drupal\commerce_price\Price $price
   *   The price.
   *
   * @return string
   *   The rendered price.
   */
  protected function buildPrice(Price $price) {
    $currency = $this->entityTypeManager
      ->getStorage('commerce_currency')
      ->load($price->getCurrencyCode());

    return [
      '#markup' => $this->numberFormatter->formatCurrency(
        $price->getNumber(),
        $currency
      ),
    ];
  }

  /**
   * Builds the render array for editing order item's quantity.
   *
   * Largely follows the way the Edit Quantity input field is built in the views
   * field plugin provided by the `commerce_cart` module.
   *
   * @param \Drupal\commerce_order\Entity\OrderItemInterface $order_item
   *   The order item.
   *
   * @return array
   *   The render array for the quantity.
   */
  protected function buildEditQuantity(OrderItemInterface $order_item) {
    $form_display = commerce_get_entity_display(
      'commerce_order_item',
      $order_item->bundle(),
      'form'
    );
    // @I Allow step and precision to be customizable
    //    type     : improvement
    //    priority : normal
    //    labels   : cart-item, form
    //    notes    : That should be achieved by loading the custom form mode's
    //               settings when the database serialization issue that occurs
    //               when the `inline_entity_form` widget is used is resolved.
    $quantity_component = $form_display->getComponent('quantity');
    $step = $quantity_component['settings']['step'];
    $precision = $step >= '1' ? 0 : strlen($step) - 2;

    return [
      '#type' => 'number',
      '#title' => $this->t('Quantity'),
      '#title_display' => 'invisible',
      '#default_value' => round($order_item->getQuantity(), $precision),
      '#size' => 4,
      '#min' => 0,
      '#max' => 9999,
      '#step' => $step,
      '#required' => TRUE,
    ];
  }

  /**
   * Returns whether the form is a cart item form.
   *
   * These form alterations are only meant to be used in the cart item page
   * form.
   *
   * @param array &$form
   *   Nested array of form elements that comprise the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   * @param string $form_id
   *   String representing the name of the form itself.
   *
   * @return bool
   *   Whether the form is a cart item form.
   */
  protected function isCartItemForm(
    array &$form,
    FormStateInterface $form_state,
    $form_id
  ) {
    // Make sure we're on an order item form.
    if (strpos($form_id, 'commerce_order_item') !== 0) {
      return FALSE;
    }

    $form_object = $form_state->getFormObject();
    if (!$form_object instanceof ContentEntityFormInterface) {
      return FALSE;
    }

    $order_item = $form_object->getEntity();
    if (!$order_item instanceof OrderItemInterface) {
      return FALSE;
    }

    // Make sure we're on the cart item edit page - there could be other pages
    // using the same form mode.
    $route_name = $this->routeMatch->getRouteName();
    if ($route_name !== 'commerce_cart_advanced.cart_item_page') {
      return FALSE;
    }

    return TRUE;
  }

  /**
   * Gets the order item we are editing from the form state.
   *
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   *
   * @return \Drupal\commerce_order\Entity\OrderItemInterface
   *   The form's order item.
   */
  protected function getOrderItem(FormStateInterface $form_state) {
    return $form_state->getFormObject()->getEntity();
  }

}

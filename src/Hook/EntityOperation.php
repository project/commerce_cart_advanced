<?php

namespace Drupal\commerce_cart_advanced\Hook;

use Drupal\commerce_order\Entity\OrderInterface;

use Drupal\Core\Entity\EntityAccessControlHandlerInterface;
use Drupal\Core\Session\AccountProxyInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\StringTranslation\TranslationInterface;
use Drupal\Core\Url;

/**
 * Holds methods implementing hooks related to cart order operations.
 */
class EntityOperation {

  use StringTranslationTrait;

  /**
   * The cart access control handler.
   *
   * @var \Drupal\Core\Entity\EntityAccessControlHandlerInterface
   */
  protected $cartAccessHandler;

  /**
   * The current user account.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  protected $account;

  /**
   * Constructs a new CartOperation object.
   *
   * @param \Drupal\Core\Session\AccountProxyInterface $account_proxy
   *   The current user account proxy.
   * @param \Drupal\Core\Entity\EntityAccessControlHandlerInterface $cart_access_handler
   *   The cart access control handler.
   * @param \Drupal\Core\StringTranslation\TranslationInterface $string_translation
   *   The string translation service.
   */
  public function __construct(
    AccountProxyInterface $account_proxy,
    EntityAccessControlHandlerInterface $cart_access_handler,
    TranslationInterface $string_translation
  ) {
    $this->account = $account_proxy->getAccount();
    $this->cartAccessHandler = $cart_access_handler;
    $this->stringTranslation = $string_translation;
  }

  /**
   * Implements hook_entity_operation().
   *
   * Adds an operation that links to the cart view page.
   *
   * @param \Drupal\commerce_order\Entity\OrderInterface $cart
   *   The order entity for the cart.
   *
   * @return array
   *   An associative array of operation link data.
   *   See \Drupal\Core\Entity\EntityListBuilderInterface::getOperations().
   */
  public function cartOperation(OrderInterface $cart) {
    if (!$cart->get('cart')->value) {
      return [];
    }

    $has_permission = $this->cartAccessHandler->access(
      $cart,
      'update',
      $this->account
    );
    if (!$has_permission) {
      return [];
    }

    return [
      'view_cart' => [
        'title' => $this->t('View cart'),
        'url' => Url::fromRoute(
          'commerce_cart_advanced.single_cart_page',
          ['cart' => $cart->id()]
        ),
      ],
    ];
  }

}
